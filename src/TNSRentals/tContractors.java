/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TNSRentals;

/**
 *
 * @author cmlane
 */
public class tContractors {
    // data members
    
     //contractors
    private String idcontractors;
    private String contractorname;
    private String phone;
    private String address;
    private String maintinvID;
    
    //Accessor Functions

    public String getIdcontractors() {
        return idcontractors;
    }

    public void setIdcontractors(String idcontractors) {
        this.idcontractors = idcontractors;
    }

    public String getContractorname() {
        return contractorname;
    }

    public void setContractorname(String contractorname) {
        this.contractorname = contractorname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMaintinvID() {
        return maintinvID;
    }

    public void setMaintinvID(String maintinvID) {
        this.maintinvID = maintinvID;
    }
    
    
    
}
