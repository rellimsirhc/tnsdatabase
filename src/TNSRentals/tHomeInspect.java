/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TNSRentals;

/**
 *
 * @author cmlane
 */
public class tHomeInspect {
    
    // data members
    
            //home inspect
    private String idhomeinspection;
    private String inspectionsheet;
    private String inspectdate;
    private String inspectlicID;
    private String ipropertyID;
    private String propertyaddress;
    
    //Accessor Functions

    public String getIdhomeinspection() {
        return idhomeinspection;
    }

    public void setIdhomeinspection(String idhomeinspection) {
        this.idhomeinspection = idhomeinspection;
    }

    public String getInspectionsheet() {
        return inspectionsheet;
    }

    public void setInspectionsheet(String inspectionsheet) {
        this.inspectionsheet = inspectionsheet;
    }

    public String getInspectdate() {
        return inspectdate;
    }

    public void setInspectdate(String inspectdate) {
        this.inspectdate = inspectdate;
    }

    public String getInspectlicID() {
        return inspectlicID;
    }

    public void setInspectlicID(String inspectlicID) {
        this.inspectlicID = inspectlicID;
    }

    public String getIpropertyID() {
        return ipropertyID;
    }

    public void setIpropertyID(String ipropertyID) {
        this.ipropertyID = ipropertyID;
    }

    public String getPropertyaddress() {
        return propertyaddress;
    }

    public void setPropertyaddress(String propertyaddress) {
        this.propertyaddress = propertyaddress;
    }
    
    
    
}
