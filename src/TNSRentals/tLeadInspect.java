/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TNSRentals;

/**
 *
 * @author cmlane
 */
public class tLeadInspect {
    //data members
    
           //lead inspect
    private String idleadinspect;
    private String inspectdate;
    private String inspectdue;
    private String inspectcost;
    private String property;
    private String MDEfeepaid;
    private String leadDisclosureAck;
    private String lpropertyID;
    private String leadinspectlic;
        
     //Accessor Functions

    public String getIdleadinspect() {
        return idleadinspect;
    }

    public void setIdleadinspect(String idleadinspect) {
        this.idleadinspect = idleadinspect;
    }

    public String getInspectdate() {
        return inspectdate;
    }

    public void setInspectdate(String inspectdate) {
        this.inspectdate = inspectdate;
    }

    public String getInspectdue() {
        return inspectdue;
    }

    public void setInspectdue(String inspectdue) {
        this.inspectdue = inspectdue;
    }

    public String getInspectcost() {
        return inspectcost;
    }

    public void setInspectcost(String inspectcost) {
        this.inspectcost = inspectcost;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getMDEfeepaid() {
        return MDEfeepaid;
    }

    public void setMDEfeepaid(String MDEfeepaid) {
        this.MDEfeepaid = MDEfeepaid;
    }

    public String getLeadDisclosureAck() {
        return leadDisclosureAck;
    }

    public void setLeadDisclosureAck(String leadDisclosureAck) {
        this.leadDisclosureAck = leadDisclosureAck;
    }

    public String getLpropertyID() {
        return lpropertyID;
    }

    public void setLpropertyID(String lpropertyID) {
        this.lpropertyID = lpropertyID;
    }

    public String getLeadinspectlic() {
        return leadinspectlic;
    }

    public void setLeadinspectlic(String leadinspectlic) {
        this.leadinspectlic = leadinspectlic;
    }
    
    
}
