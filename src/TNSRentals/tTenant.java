/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TNSRentals;

/**
 *
 * @author cmlane
 */
public class tTenant {
    //data members
    //tenants
    private String idtenant;
    private String tssn;
    private String fname;
    private String midinit;
    private String lname;
    private String dob;
    private String datemovein;
    private String leaseagreement;
    private String petdeposit;
    private String pets;
    private String securitydeposit;
    private String firstmonthrent;
    private String tapplicantID;
    private String tpropertyID;
    private String propID;
    private String tcarbID;
    private String tleadID;
    
    //Accessor Functions

    public String getIdtenant() {
        return idtenant;
    }

    public void setIdtenant(String idtenant) {
        this.idtenant = idtenant;
    }

    public String getTssn() {
        return tssn;
    }

    public void setTssn(String tssn) {
        this.tssn = tssn;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMidinit() {
        return midinit;
    }

    public void setMidinit(String midinit) {
        this.midinit = midinit;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDatemovein() {
        return datemovein;
    }

    public void setDatemovein(String datemovein) {
        this.datemovein = datemovein;
    }

    public String getLeaseagreement() {
        return leaseagreement;
    }

    public void setLeaseagreement(String leaseagreement) {
        this.leaseagreement = leaseagreement;
    }

    public String getPetdeposit() {
        return petdeposit;
    }

    public void setPetdeposit(String petdeposit) {
        this.petdeposit = petdeposit;
    }

    public String getPets() {
        return pets;
    }

    public void setPets(String pets) {
        this.pets = pets;
    }

    public String getSecuritydeposit() {
        return securitydeposit;
    }

    public void setSecuritydeposit(String securitydeposit) {
        this.securitydeposit = securitydeposit;
    }

    public String getFirstmonthrent() {
        return firstmonthrent;
    }

    public void setFirstmonthrent(String firstmonthrent) {
        this.firstmonthrent = firstmonthrent;
    }

    public String getTapplicantID() {
        return tapplicantID;
    }

    public void setTapplicantID(String tapplicantID) {
        this.tapplicantID = tapplicantID;
    }

    public String getTpropertyID() {
        return tpropertyID;
    }

    public void setTpropertyID(String tpropertyID) {
        this.tpropertyID = tpropertyID;
    }

    public String getPropID() {
        return propID;
    }

    public void setPropID(String propID) {
        this.propID = propID;
    }

    public String getTcarbID() {
        return tcarbID;
    }

    public void setTcarbID(String tcarbID) {
        this.tcarbID = tcarbID;
    }

    public String getTleadID() {
        return tleadID;
    }

    public void setTleadID(String tleadID) {
        this.tleadID = tleadID;
    }
    
    
    
    
}
