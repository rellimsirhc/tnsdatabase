/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TNSRentals;

/**
 *
 * @author cmlane
 */
public class tGround {
    //data members
    
      //ground
    private String idground;
    private String ownername;
    private String amountdue;
    private String invoiceno;
    private String submitaddress;
    private String datepaid;
    private String company_office;
    private String hpropertyID;
    
    // Accessor Functions

    public String getIdground() {
        return idground;
    }

    public void setIdground(String idground) {
        this.idground = idground;
    }

    public String getOwnername() {
        return ownername;
    }

    public void setOwnername(String ownername) {
        this.ownername = ownername;
    }

    public String getAmountdue() {
        return amountdue;
    }

    public void setAmountdue(String amountdue) {
        this.amountdue = amountdue;
    }

    public String getInvoiceno() {
        return invoiceno;
    }

    public void setInvoiceno(String invoiceno) {
        this.invoiceno = invoiceno;
    }

    public String getSubmitaddress() {
        return submitaddress;
    }

    public void setSubmitaddress(String submitaddress) {
        this.submitaddress = submitaddress;
    }

    public String getDatepaid() {
        return datepaid;
    }

    public void setDatepaid(String datepaid) {
        this.datepaid = datepaid;
    }

    public String getCompany_office() {
        return company_office;
    }

    public void setCompany_office(String company_office) {
        this.company_office = company_office;
    }

    public String getHpropertyID() {
        return hpropertyID;
    }

    public void setHpropertyID(String hpropertyID) {
        this.hpropertyID = hpropertyID;
    }
    
    
    
}
