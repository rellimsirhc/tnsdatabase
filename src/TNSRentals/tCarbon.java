/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TNSRentals;

/**
 *
 * @author cmlane
 */
public class tCarbon {
    
    // data members
    //carbon
    private String idcarbon;
    private String datesigned;
    private String cbpropertyID;
    private String applicantID;
    
    //Accessor Functions

    public String getIdcarbon() {
        return idcarbon;
    }

    public void setIdcarbon(String idcarbon) {
        this.idcarbon = idcarbon;
    }

    public String getDatesigned() {
        return datesigned;
    }

    public void setDatesigned(String datesigned) {
        this.datesigned = datesigned;
    }

    public String getCbpropertyID() {
        return cbpropertyID;
    }

    public void setCbpropertyID(String cbpropertyID) {
        this.cbpropertyID = cbpropertyID;
    }

    public String getApplicantID() {
        return applicantID;
    }

    public void setApplicantID(String applicantID) {
        this.applicantID = applicantID;
    }
    
    
}
