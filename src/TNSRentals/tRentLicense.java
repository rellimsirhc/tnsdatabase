/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TNSRentals;

/**
 *
 * @author cmlane
 */
public class tRentLicense {
    //data members
    //rent license
    private String idrentlicense;
    private String licensedate;
    private String licenseexpire;
    private String licepaiddate;
    private String amountpaid;
    private String carbID;
    private String inspectLicID;
    private String leadinspectID;
    private String rpropertyID;
    private String homeInspNo;
    
    //Accessor Functions

    public String getIdrentlicense() {
        return idrentlicense;
    }

    public void setIdrentlicense(String idrentlicense) {
        this.idrentlicense = idrentlicense;
    }

    public String getLicensedate() {
        return licensedate;
    }

    public void setLicensedate(String licensedate) {
        this.licensedate = licensedate;
    }

    public String getLicenseexpire() {
        return licenseexpire;
    }

    public void setLicenseexpire(String licenseexpire) {
        this.licenseexpire = licenseexpire;
    }

    public String getLicepaiddate() {
        return licepaiddate;
    }

    public void setLicepaiddate(String licepaiddate) {
        this.licepaiddate = licepaiddate;
    }

    public String getAmountpaid() {
        return amountpaid;
    }

    public void setAmountpaid(String amountpaid) {
        this.amountpaid = amountpaid;
    }

    public String getCarbID() {
        return carbID;
    }

    public void setCarbID(String carbID) {
        this.carbID = carbID;
    }

    public String getInspectLicID() {
        return inspectLicID;
    }

    public void setInspectLicID(String inspectLicID) {
        this.inspectLicID = inspectLicID;
    }

    public String getLeadinspectID() {
        return leadinspectID;
    }

    public void setLeadinspectID(String leadinspectID) {
        this.leadinspectID = leadinspectID;
    }

    public String getRpropertyID() {
        return rpropertyID;
    }

    public void setRpropertyID(String rpropertyID) {
        this.rpropertyID = rpropertyID;
    }

    public String getHomeInspNo() {
        return homeInspNo;
    }

    public void setHomeInspNo(String homeInspNo) {
        this.homeInspNo = homeInspNo;
    }
    
    
    
}
