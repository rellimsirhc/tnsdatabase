/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TNSRentals;

/**
 *
 * @author cmlane
 */
public class tRentAccount {
    //data memebers
    
      //rent account
    private String idinvoicerent;
    private String amountdue;
    private String latefee;
    private String paid;
    private String paydate;
    private String invoicedate;
    private String previousbalance;
    private String rentpropertyID;
    private String rapplicantID;
    private String rwaterID;
 
    //Accessor Function

    public String getIdinvoicerent() {
        return idinvoicerent;
    }

    public void setIdinvoicerent(String idinvoicerent) {
        this.idinvoicerent = idinvoicerent;
    }

    public String getAmountdue() {
        return amountdue;
    }

    public void setAmountdue(String amountdue) {
        this.amountdue = amountdue;
    }

    public String getLatefee() {
        return latefee;
    }

    public void setLatefee(String latefee) {
        this.latefee = latefee;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getPaydate() {
        return paydate;
    }

    public void setPaydate(String paydate) {
        this.paydate = paydate;
    }

    public String getInvoicedate() {
        return invoicedate;
    }

    public void setInvoicedate(String invoicedate) {
        this.invoicedate = invoicedate;
    }

    public String getPreviousbalance() {
        return previousbalance;
    }

    public void setPreviousbalance(String previousbalance) {
        this.previousbalance = previousbalance;
    }

    public String getRentpropertyID() {
        return rentpropertyID;
    }

    public void setRentpropertyID(String rentpropertyID) {
        this.rentpropertyID = rentpropertyID;
    }

    public String getRapplicantID() {
        return rapplicantID;
    }

    public void setRapplicantID(String rapplicantID) {
        this.rapplicantID = rapplicantID;
    }

    public String getRwaterID() {
        return rwaterID;
    }

    public void setRwaterID(String rwaterID) {
        this.rwaterID = rwaterID;
    }
    
    
}
