/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TNSRentals;

/**
 *
 * @author cmlane
 */
public class tHomeInsurance {
    //data members
    
              //home insurance
    private String idhomeinsurance;
    private String inscompany;
    private String insamount;
    private String invoiceno;
    private String amountpaid;
    private String datepaid;
    private String hipropertyID;
    
    // Accessor Functions

    public String getIdhomeinsturance() {
        return idhomeinsurance;
    }

    public void setIdhomeinsturance(String idhomeinsturance) {
        this.idhomeinsturance = idhomeinsturance;
    }

    public String getInscompany() {
        return inscompany;
    }

    public void setInscompany(String inscompany) {
        this.inscompany = inscompany;
    }

    public String getInsamount() {
        return insamount;
    }

    public void setInsamount(String insamount) {
        this.insamount = insamount;
    }

    public String getInvoiceno() {
        return invoiceno;
    }

    public void setInvoiceno(String invoiceno) {
        this.invoiceno = invoiceno;
    }

    public String getAmountpaid() {
        return amountpaid;
    }

    public void setAmountpaid(String amountpaid) {
        this.amountpaid = amountpaid;
    }

    public String getDatepaid() {
        return datepaid;
    }

    public void setDatepaid(String datepaid) {
        this.datepaid = datepaid;
    }

    public String getHipropertyID() {
        return hipropertyID;
    }

    public void setHipropertyID(String hipropertyID) {
        this.hipropertyID = hipropertyID;
    }
    
    
    
}
