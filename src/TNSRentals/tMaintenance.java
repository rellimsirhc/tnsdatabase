/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TNSRentals;

/**
 *
 * @author cmlane
 */
public class tMaintenance {
    // data members 
    
        // maintenance
    private String idmaintenance;
    private String date;
    private String workdescript;
    private String ctrname;
    private String paid;
    private String propertyID;
    private String ctrID;
    
    //Accessor Functions

    public String getIdmaintenance() {
        return idmaintenance;
    }

    public void setIdmaintenance(String idmaintenance) {
        this.idmaintenance = idmaintenance;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getWorkdescript() {
        return workdescript;
    }

    public void setWorkdescript(String workdescript) {
        this.workdescript = workdescript;
    }

    public String getCtrname() {
        return ctrname;
    }

    public void setCtrname(String ctrname) {
        this.ctrname = ctrname;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getPropertyID() {
        return propertyID;
    }

    public void setPropertyID(String propertyID) {
        this.propertyID = propertyID;
    }

    public String getCtrID() {
        return ctrID;
    }

    public void setCtrID(String ctrID) {
        this.ctrID = ctrID;
    }

  
    
}
