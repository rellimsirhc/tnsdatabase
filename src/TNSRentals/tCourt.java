/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TNSRentals;

/**
 *
 * @author cmlane
 */
public class tCourt {
    
    // data members
    
        // court
    private String idcourt;
    private String filingid;
    private String filingdate;
    private String courtdate;
    private String results;
    private String ctenantSSN;
    private String cpropertyID;
 
     //Accessor Functions

    public String getIdcourt() {
        return idcourt;
    }

    public void setIdcourt(String idcourt) {
        this.idcourt = idcourt;
    }

    public String getFilingid() {
        return filingid;
    }

    public void setFilingid(String filingid) {
        this.filingid = filingid;
    }

    public String getFilingdate() {
        return filingdate;
    }

    public void setFilingdate(String filingdate) {
        this.filingdate = filingdate;
    }

    public String getCourtdate() {
        return courtdate;
    }

    public void setCourtdate(String courtdate) {
        this.courtdate = courtdate;
    }

    public String getResults() {
        return results;
    }

    public void setResults(String results) {
        this.results = results;
    }

    public String getCtenantSSN() {
        return ctenantSSN;
    }

    public void setCtenantSSN(String ctenantSSN) {
        this.ctenantSSN = ctenantSSN;
    }

    public String getCpropertyID() {
        return cpropertyID;
    }

    public void setCpropertyID(String cpropertyID) {
        this.cpropertyID = cpropertyID;
    }
    
    
    
}
