/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TNSRentals;

/**
 *
 * @author cmlane
 */
public class tInspector {
    
    //data members
    //inspectors
    private String idinspector;
    private String licenseno;
    private String company;
    private String name;
    private String leadinspectID;
    private String homeinspectID;
       
     //Accessor Functions

    public String getIdinspector() {
        return idinspector;
    }

    public void setIdinspector(String idinspector) {
        this.idinspector = idinspector;
    }

    public String getLicenseno() {
        return licenseno;
    }

    public void setLicenseno(String licenseno) {
        this.licenseno = licenseno;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLeadinspectID() {
        return leadinspectID;
    }

    public void setLeadinspectID(String leadinspectID) {
        this.leadinspectID = leadinspectID;
    }

    public String getHomeinspectID() {
        return homeinspectID;
    }

    public void setHomeinspectID(String homeinspectID) {
        this.homeinspectID = homeinspectID;
    }
   
    
}
