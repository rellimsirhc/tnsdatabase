/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TNSRentals;

/**
 *
 * @author cmlane
 */
public class tProperty {
       //data members
    //property
    private String idproperty;
    private String street;
    private String city;
    private String state;
    private String zipcode;
    private String rent;
    private String securitydep;
    private String petdet;
    private String type;
    private String yearbuilt;
    private String squarefoot;
    private String bathrooms;
    private String bedrooms;
    private String lotsize;
    private String appID;
    private String homeins;
    private String carbonID;
    private String leadID;
    private String maintID;
    private String taxID;
    private String rentlicID;
    private String homeinspno;
     
    //Accessor Functions

    public String getIdproperty() {
        return idproperty;
    }

    public void setIdproperty(String idproperty) {
        this.idproperty = idproperty;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getRent() {
        return rent;
    }

    public void setRent(String rent) {
        this.rent = rent;
    }

    public String getSecuritydep() {
        return securitydep;
    }

    public void setSecuritydep(String securitydep) {
        this.securitydep = securitydep;
    }

    public String getPetdet() {
        return petdet;
    }

    public void setPetdet(String petdet) {
        this.petdet = petdet;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getYearbuilt() {
        return yearbuilt;
    }

    public void setYearbuilt(String yearbuilt) {
        this.yearbuilt = yearbuilt;
    }

    public String getSquarefoot() {
        return squarefoot;
    }

    public void setSquarefoot(String squarefoot) {
        this.squarefoot = squarefoot;
    }

    public String getBathrooms() {
        return bathrooms;
    }

    public void setBathrooms(String bathrooms) {
        this.bathrooms = bathrooms;
    }

    public String getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(String bedrooms) {
        this.bedrooms = bedrooms;
    }

    public String getLotsize() {
        return lotsize;
    }

    public void setLotsize(String lotsize) {
        this.lotsize = lotsize;
    }

    public String getAppID() {
        return appID;
    }

    public void setAppID(String appID) {
        this.appID = appID;
    }

    public String getHomeins() {
        return homeins;
    }

    public void setHomeins(String homeins) {
        this.homeins = homeins;
    }

    public String getCarbonID() {
        return carbonID;
    }

    public void setCarbonID(String carbonID) {
        this.carbonID = carbonID;
    }

    public String getLeadID() {
        return leadID;
    }

    public void setLeadID(String leadID) {
        this.leadID = leadID;
    }

    public String getMaintID() {
        return maintID;
    }

    public void setMaintID(String maintID) {
        this.maintID = maintID;
    }

    public String getTaxID() {
        return taxID;
    }

    public void setTaxID(String taxID) {
        this.taxID = taxID;
    }

    public String getRentlicID() {
        return rentlicID;
    }

    public void setRentlicID(String rentlicID) {
        this.rentlicID = rentlicID;
    }

    public String getHomeinspno() {
        return homeinspno;
    }

    public void setHomeinspno(String homeinspno) {
        this.homeinspno = homeinspno;
    }
    
    
        
}
