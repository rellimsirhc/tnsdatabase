/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TNSRentals;

/**
 *
 * @author cmlane
 */
public class tApplicant {
          //data members
    //applicant
    private String idapplicant;
    private String fName;
    private String lname;
    private String midinit;
    private String empl_address;
    private String emplphone;
    private String emplstarted;
    private String incomeretire;
    private String incomegross;
    private String incomeother;
    private String ssn;
    private String email;
    private String cellphone;
    private String propID;
    
    //Accessor Functions

    public String getIdapplicant() {
        return idapplicant;
    }

    public void setIdapplicant(String idapplicant) {
        this.idapplicant = idapplicant;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getMidinit() {
        return midinit;
    }

    public void setMidinit(String midinit) {
        this.midinit = midinit;
    }

    public String getEmpl_address() {
        return empl_address;
    }

    public void setEmpl_address(String empl_address) {
        this.empl_address = empl_address;
    }

    public String getEmplphone() {
        return emplphone;
    }

    public void setEmplphone(String emplphone) {
        this.emplphone = emplphone;
    }

    public String getEmplstarted() {
        return emplstarted;
    }

    public void setEmplstarted(String emplstarted) {
        this.emplstarted = emplstarted;
    }

    public String getIncomeretire() {
        return incomeretire;
    }

    public void setIncomeretire(String incomeretire) {
        this.incomeretire = incomeretire;
    }

    public String getIncomegross() {
        return incomegross;
    }

    public void setIncomegross(String incomegross) {
        this.incomegross = incomegross;
    }

    public String getIncomeother() {
        return incomeother;
    }

    public void setIncomeother(String incomeother) {
        this.incomeother = incomeother;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getPropID() {
        return propID;
    }

    public void setPropID(String propID) {
        this.propID = propID;
    }
    
    
}
